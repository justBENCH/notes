import axios from 'axios';
import queryString from 'query-string';

class Api {
	request = (endpoint, types = [], data, options = {}, method) => {
		const {
			onFailure,
			onSuccess,
		} = options;

		const [REQUEST, SUCCESS, FAILURE] = types;

		let params;
		switch (method) {
			case 'get':
				params = {
					url: `${process.env.REACT_APP_API_URL}${endpoint}${data ? '?' + queryString.stringify(data) : ''}`,
					method,
				};
				break;
			default:
				params = {
					url: `${process.env.REACT_APP_API_URL}${endpoint}`,
					method,
					data,
				};
				break;
		}

		return (dispatch) => {
			if (REQUEST) {
				dispatch({
					type: REQUEST.type || REQUEST,
					payload: REQUEST.payload || REQUEST,
				});
			}

			axios.request(params)
				.then((response) => {
					if (SUCCESS) {
						dispatch({
							type: SUCCESS.type || SUCCESS,
							payload: SUCCESS.payload ?
								SUCCESS.payload(response.data) :
								response.data,
						});
					}

					if (onSuccess) {
						onSuccess(response.data);
					}
				})
				.catch((error) => {
					if (FAILURE) {
						dispatch({
							type: FAILURE.type || FAILURE,
							payload: FAILURE.payload ?
								FAILURE.payload(error) :
								error,
						});
					}

					if (onFailure) {
						onFailure(error.message, error.status);
					}
				});
		};
	};

	get = (endpoint, types, data, options) => this.request(
		endpoint,
		types,
		data,
		options,
		'get',
	);

	post = (endpoint, types, data, options) => this.request(
		endpoint,
		types,
		data,
		options,
		'post',
	);

	put = (endpoint, types, data, options) => this.request(
		endpoint,
		types,
		data,
		options,
		'put',
	);

	delete = (endpoint, types, data, options) => this.request(
		endpoint,
		types,
		data,
		options,
		'delete',
	);
}

const api = new Api();

export default api;
