import { useDispatch } from 'react-redux';
import api from './api';
import { notesTypes } from '../constants/notesConstants';
import { tagsTypes } from '../constants/tagsConstants';

const useApi = () => {
	const dispatch = useDispatch();

	// NOTES
	const requestGetNotes = () => new Promise((resolve, reject) => {
		dispatch(
			api.get(
				'notes',
				[
					notesTypes.GET_NOTES_REQUEST,
					notesTypes.GET_NOTES_SUCCESS,
					notesTypes.GET_NOTES_FAILURE,
				],
				null,
				{
					onSuccess: resolve,
					onFailure: reject,
				},
			),
		);
	});

	const requestCreateNote = () => new Promise((resolve, reject) => {
		dispatch(
			api.post(
				'notes',
				[
					notesTypes.CREATE_NOTE_REQUEST,
					notesTypes.CREATE_NOTE_SUCCESS,
					notesTypes.CREATE_NOTE_FAILURE,
				],
				{ text: '' },
				{
					onSuccess: resolve,
					onFailure: reject,
				},
			),
		);
	});

	const requestUpdateNote = (id, text) => new Promise((resolve, reject) => {
		dispatch(
			api.put(
				`notes/${id}`,
				[
					notesTypes.UPDATE_NOTE_REQUEST,
					notesTypes.UPDATE_NOTE_SUCCESS,
					notesTypes.UPDATE_NOTE_FAILURE,
				],
				{ id, text },
				{
					onSuccess: resolve,
					onFailure: reject,
				},
			),
		);
	});

	const requestDeleteNote = (id) => new Promise((resolve, reject) => {
		dispatch(
			api.delete(
				`notes/${id}`,
				[
					notesTypes.UPDATE_NOTE_REQUEST,
					notesTypes.UPDATE_NOTE_SUCCESS,
					notesTypes.UPDATE_NOTE_FAILURE,
				],
				null,
				{
					onSuccess: resolve,
					onFailure: reject,
				},
			),
		);
	});

	// TAGS
	const requestGetTags = () => new Promise((resolve, reject) => {
		dispatch(
			api.get(
				'tags',
				[
					tagsTypes.GET_TAGS_REQUEST,
					tagsTypes.GET_TAGS_SUCCESS,
					tagsTypes.GET_TAGS_FAILURE,
				],
				null,
				{
					onSuccess: resolve,
					onFailure: reject,
				},
			),
		);
	});

	const requestCreateTag = (name) => new Promise((resolve, reject) => {
		dispatch(
			api.post(
				'tags',
				[
					tagsTypes.CREATE_TAG_REQUEST,
					tagsTypes.CREATE_TAG_SUCCESS,
					tagsTypes.CREATE_TAG_FAILURE,
				],
				{ name },
				{
					onSuccess: resolve,
					onFailure: reject,
				},
			),
		);
	});

	const requestUpdateTag = (id, text) => new Promise((resolve, reject) => {
		dispatch(
			api.put(
				`tags/${id}`,
				[
					tagsTypes.UPDATE_TAG_REQUEST,
					tagsTypes.UPDATE_TAG_SUCCESS,
					tagsTypes.UPDATE_TAG_FAILURE,
				],
				{ id, text },
				{
					onSuccess: resolve,
					onFailure: reject,
				},
			),
		);
	});

	const requestDeleteTag = (id) => new Promise((resolve, reject) => {
		dispatch(
			api.delete(
				`tags/${id}`,
				[
					tagsTypes.UPDATE_TAG_REQUEST,
					tagsTypes.UPDATE_TAG_SUCCESS,
					tagsTypes.UPDATE_TAG_FAILURE,
				],
				null,
				{
					onSuccess: resolve,
					onFailure: reject,
				},
			),
		);
	});

	return ({
		requestGetNotes,
		requestCreateNote,
		requestUpdateNote,
		requestDeleteNote,

		requestGetTags,
		requestCreateTag,
		requestUpdateTag,
		requestDeleteTag,
	});
};

export default useApi;
