import {
  Button, Card, Space, Tag,
} from 'antd';
import { useState } from 'react';
import Icon from '@mdi/react';
import { mdiWindowClose } from '@mdi/js';
import { Mention, MentionsInput } from 'react-mentions';
import PropTypes from 'prop-types';

function NoteCard({
  text, id, onDelete, tags, onUpdate,
}) {
  const tagsRegex = new RegExp(`${tags.map(({ name }) => `#${name}`).join('|')}`, 'g');

  const textToRender = (value) => {
    if (tags.length > 0) {
      return value.replaceAll(tagsRegex, (tag) => `@[${tag}](${tag.slice(1)})`);
    }
    return value;
  };

  const [textValue, setTextValue] = useState(text);
  const [renderTextValue, setRenderTextValue] = useState(textToRender(text));

  const onChange = (_e, _renderText, value) => {
    setRenderTextValue(textToRender(value));
    setTextValue(value);
  };

  const closeButton = (
    <Button
      onClick={() => onDelete(id)}
      className="button_close"
    >
      <Icon path={mdiWindowClose} size={0.7} />
    </Button>
  );

  const getCardTags = () => {
    const strTags = textValue.match(tagsRegex);
    if (tags.length > 0 && strTags) {
      const uniqTags = new Set(strTags);
      return [...uniqTags].map((item) => <Tag key={item} color="#faad14">{item.slice(1)}</Tag>);
    }

    return null;
  };

  const onBlur = () => {
    onUpdate(id, textValue);
  };

  return (
    <Card
      className="note_card"
      hoverable
    >
      <div className="d-flex">
        {closeButton}
      </div>
      <div className="note_body">
        <MentionsInput
          value={renderTextValue}
          onChange={onChange}
          onBlur={onBlur}
        >
          <Mention
            className="mention"
            trigger="#"
            data={tags.map(({ name }) => ({ id: name, display: `#${name}` }))}
          />
        </MentionsInput>
        <Space className="mt-3" size={[0, 0]}>
          {getCardTags()}
        </Space>
      </div>
    </Card>
  );
}

NoteCard.propTypes = {
  text: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  onDelete: PropTypes.func.isRequired,
  tags: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
  })).isRequired,
  onUpdate: PropTypes.func.isRequired,
};

export default NoteCard;
