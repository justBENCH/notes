import {
  Button, Col, Dropdown, Input, Menu, Row, Select,
} from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import Icon from '@mdi/react';
import { mdiPlus } from '@mdi/js';
import PropTypes from 'prop-types';
import useApi from '../api/useApi';

function TagsFilter({ tags, onSelectFilters, selectedValues }) {
  const { requestCreateTag, requestGetTags, requestDeleteTag } = useApi();
  const ref = useRef();

  const [addMode, setAddMode] = useState(false);
  const [newTagValue, setNewTagValue] = useState('');

  const toggleAddMode = () => setAddMode((prev) => !prev);

  const onAddNewTag = async () => {
    if (newTagValue) {
      const tagNames = tags.map(({ name }) => name);
      if (!tagNames.includes(newTagValue)) {
        await requestCreateTag(newTagValue);
        requestGetTags();
      }
    }
    toggleAddMode();
  };

  const onChangeFilter = (values) => {
    onSelectFilters(values);
  };

  const onDeleteTag = async ({ key }) => {
    onSelectFilters([]);
    await requestDeleteTag(key);
    requestGetTags();
  };

  const deleteMenu = (
    <Menu
      onClick={onDeleteTag}
      items={tags.map(({ name, id }) => ({ key: id, label: name }))}
    />
  );

  useEffect(() => {
    if (ref.current && addMode) {
      ref.current.focus();
    }
  }, [addMode]);

  return (
    <Row gutter={[6, 0]}>
      <Col span={6}>
        <Select
          mode="multiple"
          placeholder="Select filter tags"
          allowClear
          size="small"
          options={tags.map(({ name }) => ({ label: name, value: name }))}
          style={{ width: '100%' }}
          onChange={onChangeFilter}
          value={selectedValues}
        />
      </Col>
      <Col>
        {addMode ? (
          <Input
            ref={ref}
            size="small"
            onChange={(e) => setNewTagValue(e.target.value)}
            onBlur={onAddNewTag}
            onPressEnter={onAddNewTag}
          />
        ) : (
          <Button
            type="dashed"
            size="small"
            onClick={toggleAddMode}
          >
            <Icon path={mdiPlus} size={0.6} />
            add tag
          </Button>
        )}
      </Col>
      <Col>
        <Dropdown
          overlay={deleteMenu}
          placement="bottomLeft"
          trigger={['click']}
          arrow
        >
          <Button
            type="dashed"
            size="small"
            danger
          >
            delete tags
          </Button>
        </Dropdown>
      </Col>
    </Row>
  );
}

TagsFilter.propTypes = {
  tags: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
  })).isRequired,
  onSelectFilters: PropTypes.func.isRequired,
  selectedValues: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default TagsFilter;
