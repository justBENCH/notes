import React, { useEffect, useState } from 'react';
import { Button, Col, Row } from 'antd';
import { useSelector } from 'react-redux';
import useApi from './api/useApi';
import { getNotes, getTags } from './store/selectors';
import NoteCard from './components/NoteCard';
import TagsFilter from './components/TagsFilter';

function App() {
  const {
    requestGetNotes, requestCreateNote, requestDeleteNote, requestGetTags, requestUpdateNote,
  } = useApi();

  const [filters, setFilters] = useState([]);

  const allNotes = useSelector(getNotes);
  const allTags = useSelector(getTags);

  const onCreateNote = () => {
    requestCreateNote();
  };

  const onDeleteNote = (id) => {
    requestDeleteNote(id)
      .then(requestGetNotes);
  };

  const onUpdateNote = (id, text) => {
    requestUpdateNote(id, text);
  };

  const initFunc = async () => {
    await requestGetTags();
    requestGetNotes();
  };

  const onSelectFilters = (values) => {
    setFilters(values);
  };

  const getFilteredNotes = () => {
    if (filters.length > 0) {
      const filterRegex = new RegExp(`${filters.map((item) => `#${item}`).join('|')}`, 'g');
      return allNotes.filter(({ text }) => text.match(filterRegex))
    }

    return allNotes;
  }

  useEffect(() => {
    initFunc();
  }, []);

  return (
    <div className="main_wrapper">
      <Row className="mb-3">
        <Col span={24}>
          <TagsFilter
            tags={allTags}
            selectedValues={filters}
            onSelectFilters={onSelectFilters}
          />
        </Col>
      </Row>
      <Row gutter={[10, 0]}>
        {getFilteredNotes().map(({ text, id }) => (
          <Col span={6} key={id}>
            <NoteCard
              tags={allTags}
              text={text}
              id={id}
              onDelete={onDeleteNote}
              onUpdate={onUpdateNote}
            />
          </Col>
        ))}
      </Row>
      <Button
        className="button_add"
        onClick={onCreateNote}
      >
        ADD NOTE
      </Button>
    </div>
  );
}

export default App;
