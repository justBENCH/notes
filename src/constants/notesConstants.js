import keyMirror from 'keymirror';

export const notesTypes = keyMirror({
    GET_NOTES_REQUEST: null,
    GET_NOTES_SUCCESS: null,
    GET_NOTES_FAILURE: null,

    CREATE_NOTE_REQUEST: null,
    CREATE_NOTE_SUCCESS: null,
    CREATE_NOTE_FAILURE: null,

    UPDATE_NOTE_REQUEST: null,
    UPDATE_NOTE_SUCCESS: null,
    UPDATE_NOTE_FAILURE: null,

    DELETE_NOTE_REQUEST: null,
    DELETE_NOTE_SUCCESS: null,
    DELETE_NOTE_FAILURE: null,
})
