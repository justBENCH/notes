import keyMirror from 'keymirror';

export const tagsTypes = keyMirror({
    GET_TAGS_REQUEST: null,
    GET_TAGS_SUCCESS: null,
    GET_TAGS_FAILURE: null,

    CREATE_TAG_REQUEST: null,
    CREATE_TAG_SUCCESS: null,
    CREATE_TAG_FAILURE: null,

    UPDATE_TAG_REQUEST: null,
    UPDATE_TAG_SUCCESS: null,
    UPDATE_TAG_FAILURE: null,

    DELETE_TAG_REQUEST: null,
    DELETE_TAG_SUCCESS: null,
    DELETE_TAG_FAILURE: null,
})
