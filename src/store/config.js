import { applyMiddleware, createStore } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rootReducer from './rootReducer';

export default function configureStore(preloadedState) {
	const middlewares = [thunkMiddleware];

	if (process.env.NODE_ENV === 'development') {
		middlewares.push(createLogger({collapsed: true}))
	}

	const middlewareEnhancer = applyMiddleware(...middlewares);
	const store = createStore(rootReducer, preloadedState, middlewareEnhancer);

	return store;
}