import { tagsTypes } from '../../constants/tagsConstants';

export const tagsReducer = (
	state = {
		data: [],
	}, {payload, type},
) => {
	switch (type) {
		case tagsTypes.GET_TAGS_SUCCESS:
			return {
				...state,
				data: payload,
			};

		case tagsTypes.CREATE_TAG_SUCCESS:
			return {
				...state,
				data: [...state.data, payload],
			};

		default:
			return state;
	}
};
