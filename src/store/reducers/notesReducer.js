import { notesTypes } from '../../constants/notesConstants';

export const notesReducer = (
	state = {
		data: [],
	}, { payload, type },
) => {
	switch (type) {
		case notesTypes.GET_NOTES_SUCCESS:
			return {
				...state,
				data: payload,
			};

		case notesTypes.CREATE_NOTE_SUCCESS:
			return {
				...state,
				data: [...state.data, payload],
			};

		case notesTypes.UPDATE_NOTE_SUCCESS: {
			const { id, text } = payload;

			return {
				...state,
				data: state.data.map((item) => item.id === id ? ({ id, text }) : item),
			};
		}
		default:
			return state;
	}
};
