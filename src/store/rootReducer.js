import { combineReducers } from '@reduxjs/toolkit';
import { notesReducer } from './reducers/notesReducer';
import { tagsReducer } from './reducers/tagsReducer';

const rootReducer = combineReducers({
	notes: notesReducer,
	tags: tagsReducer,
});

export default rootReducer;
