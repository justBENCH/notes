import _ from 'lodash';

export const getNotes = (state) => _.get(state, 'notes.data');
export const getTags = (state) => _.get(state, 'tags.data');